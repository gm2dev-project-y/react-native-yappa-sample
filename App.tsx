/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * Generated with the TypeScript template
 * https://github.com/react-native-community/react-native-template-typescript
 *
 * @format
 */

import React, { Component } from 'react';

import { StyleSheet, View, Text, TextInput } from 'react-native';
import YappaSDK, { YappaActionButton } from 'react-native-yappasdk-app';
import { Appearance } from 'react-native';


/// Component Approach
class App extends Component {
  state = {
    url: "https://variety.com/",
    contentId : "1235070678"
  };

  componentDidMount() {
    YappaSDK.initialize('86d1968a7fe9e13f68825e057028abf7', '1'); // AppID
    console.log('SDK', 'Yappa initialization');
    (async () => {
            try {
              YappaSDK.setContentId(this.state.contentId)
              YappaSDK.setContentUrl(this.state.url)
              this.handleNotifications();
            } catch (e) {
              YappaSDK.setContentId(this.state.contentId)
              YappaSDK.setContentUrl(this.state.url)
              this.handleNotifications();
            }
          })();
  }

    handleNotifications() {
      YappaSDK.handleRemoteNotification(
        (contentId: string, contentUrl: string, showSdk) => {
          console.log('SDK', 'handleRemoteNotification');
          console.log(' @@@@@@ HandleNotifications Received ', contentId, contentUrl);
          // Handle app navigation
          YappaSDK.setContentId(contentId)
          showSdk();
        }
      );
  }

  onUrlChange = async (text: any) => {
      YappaSDK.setContentUrl(text);
  };

   onContentIdChange = async (text: any) => {
    YappaSDK.setContentId(text);
  };

  render() {
    return (
      <View style={styles.container}>
      <Text>YappaSDK React Native</Text>
       <TextInput
        style={styles.input}
       onChangeText={this.onUrlChange}
        value= {this.state.url}
      /> 
    <TextInput
        style={styles.input}
        onChangeText={this.onContentIdChange}
        value= {this.state.contentId}
      /> 
      <YappaActionButton
        contentUrl= {this.state.url} //{currentUrl} //https://variety.com/
        contentId= {this.state.contentId} //{contentId}//1235070678
      />
    </View>
    );
  }
}

export default App;

// Function Approach
// export default function App() {
//   const DEFAULT_URL = 'https://variety.com/';
//   const DEFAULT_CONTENT_ID = "1235070678";
//   const [currentUrl, setCurrentUrl] = React.useState(DEFAULT_URL);
//   const [contentId, setContentId] = React.useState(DEFAULT_CONTENT_ID);


//   React.useEffect(() => {
//     YappaSDK.initialize('86d1968a7fe9e13f68825e057028abf7', '1'); // AppID
//     console.log('SDK', 'Yappa initialization');

//     (async () => {
//       const url = DEFAULT_URL;
//       const contentId = DEFAULT_CONTENT_ID;
//       try {
//         setCurrentUrl(url);
//         onContentIdChange(contentId)
//         handleNotifications();

//       } catch (e) {
//         setCurrentUrl(url);
//         onContentIdChange(contentId)
//         handleNotifications();
//       }
//     })();
//   }, []);


//   const handleNotifications = () => {
//       YappaSDK.handleRemoteNotification(
//         (contentId: string, contentUrl: string, showSdk) => {
//           console.log('SDK', 'handleRemoteNotification');
//           console.log(' @@@@@@ HandleNotifications Received ', contentId, contentUrl);
//           // Handle app navigation
//           YappaSDK.setContentId(contentId)
//           showSdk();
//         }
//       );
//   }

  
//   const onUrlChange = async (text: any) => {
//     setCurrentUrl(text);
//   };

//   const onContentIdChange = async (text: any) => {
//     setContentId(text);
//   };

//   return (
//     <View style={styles.container}>
//       <Text>YappaSDK React Native</Text>
//       <TextInput
//         style={styles.input}
//         onChangeText={onUrlChange}
//         value={currentUrl}
//       />
//        <TextInput
//         style={styles.input}
//         onChangeText={onContentIdChange}
//         value={contentId}
//       />
//       <YappaActionButton
//         contentUrl= "https://variety.com/" //{currentUrl} //https://variety.com/
//         contentId= "1235070678" //{contentId}//1235070678
//       />
//     </View>
//   );
// }

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#FFFFFF'
  },
  box: {
    width: 60,
    height: 60,
    marginVertical: 20,
  },
  input: {
    width: "90%",
    height: 40,
    margin: 12,
    borderWidth: 1,
  },
});
