### React Native Yappa SDK Test Project  ###

This a Yappa React native test project that is used to test SDK integration on React-native envirornment.

### Setup ###
In the next lines, the steps to follow will be detailed in order to carry out the correct setup of the project to be able to run it.

1.  Clone this repository
2.  Install dependecies.

    npm install react-native-yappasdk-app

    npm install @react-native-firebase/app
    npm install @react-native-firebase/messaging
    npm install @react-native-community/push-notification-ios

3. IOS extra step
    go to ios folder and run "pod install"
    
    


## License
MIT

Yappa World Inc.

